/*
   Copyright (c) 2023 Dotsenko A. N.

   This file is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 2.1 of the
   License, or (at your option) any later version.

   This file is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "gdbprovider/gdbprovider.h"

#include <sqlite3.h>

static void g_db_provider_class_init(GDbProviderClass *self_class);
static void g_db_provider_init(GDbProvider *self);

struct GDbProviderPrivate {
    GHashTable *id_by_column;
    guint next_id;
};

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE(GDbProvider, g_db_provider, G_TYPE_OBJECT);

static void g_db_provider_class_init(GDbProviderClass *self_class)
{
}

static void g_db_provider_init(GDbProvider *self)
{
    GDbProviderPrivate *priv = g_db_provider_get_instance_private(self);
    priv->id_by_column =
            g_hash_table_new_full(g_str_hash, g_str_equal, g_free, NULL);
    priv->next_id = 1;
    self->priv = priv;
}

gboolean
g_db_provider_open_db(GDbProvider *self, const char *db_name, GError **error)
{
    return G_DB_PROVIDER_GET_CLASS(self)->open_db(self, db_name, error);
}

gboolean g_db_provider_close_db(GDbProvider *self, GError **error)
{
    return G_DB_PROVIDER_GET_CLASS(self)->close_db(self, error);
}

gboolean
g_db_provider_exec_sql(GDbProvider *self, const char *sql, GError **error)
{
    return G_DB_PROVIDER_GET_CLASS(self)->exec_sql(self, sql, error);
}

gboolean g_db_provider_append_list_store_by_sql(GDbProvider *self,
                                                const char *sql,
                                                GListStore *store,
                                                GError **error)
{
    return G_DB_PROVIDER_GET_CLASS(self)->append_list_store_by_sql(
            self, sql, store, error);
}

guint g_db_provider_get_id_by_col_name(GDbProvider *self, const gchar *col_name)
{
    GDbProviderPrivate *priv = self->priv;
    guint *ptr = g_hash_table_lookup(priv->id_by_column, col_name);
    if (!ptr) {
        guint col_id = priv->next_id;
        gchar *col_name_copy = g_strdup(col_name);
        g_assert_true(g_hash_table_insert(
                priv->id_by_column, col_name_copy, GUINT_TO_POINTER(col_id)));
        ++priv->next_id;
        return col_id;
    }
    return GPOINTER_TO_UINT(ptr);
}
