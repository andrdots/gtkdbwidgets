/*
   Copyright (c) 2023 Dotsenko A. N.

   This file is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 2.1 of the
   License, or (at your option) any later version.

   This file is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef G_DB_PROVIDER_
#define G_DB_PROVIDER_

#include "gdbprovider/gdbrow.h"

#include <gio/gio.h>

G_BEGIN_DECLS

#define GTK_TYPE_DB_PROVIDER (g_db_provider_get_type())
#define G_DB_PROVIDER(obj)                                                     \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), GTK_TYPE_DB_PROVIDER, GDbProvider))
#define G_DB_PROVIDER_CLASS(klass)                                             \
    (G_TYPE_CHECK_CLASS_CAST((klass), GTK_TYPE_DB_PROVIDER, GDbProviderClass))
#define GTK_IS_DB_PROVIDER(obj)                                                \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), GTK_TYPE_DB_PROVIDER))
#define GTK_IS_DB_PROVIDER_CLASS(klass)                                        \
    (G_TYPE_CHECK_CLASS_TYPE((klass), GTK_TYPE_DB_PROVIDER))
#define G_DB_PROVIDER_GET_CLASS(obj)                                           \
    (G_TYPE_INSTANCE_GET_CLASS((obj), GTK_TYPE_DB_PROVIDER, GDbProviderClass))

typedef struct GDbProvider GDbProvider;
typedef struct GDbProviderClass GDbProviderClass;
typedef struct GDbProviderPrivate GDbProviderPrivate;

struct GDbProviderClass {
    GObjectClass parent_class;

    gboolean (*open_db)(GDbProvider *self, const char *db_name, GError **error);
    gboolean (*close_db)(GDbProvider *self, GError **error);
    gboolean (*exec_sql)(GDbProvider *self_dbprovider,
                         const char *sql,
                         GError **error);
    gboolean (*append_list_store_by_sql)(GDbProvider *self,
                                         const char *sql,
                                         GListStore *store,
                                         GError **error);
};

struct GDbProvider {
    GObject parent;

    GDbProviderPrivate *priv;
};

gboolean
g_db_provider_open_db(GDbProvider *self, const char *db_name, GError **error);

gboolean g_db_provider_close_db(GDbProvider *self, GError **error);

gboolean
g_db_provider_exec_sql(GDbProvider *self, const char *sql, GError **error);

gboolean g_db_provider_append_list_store_by_sql(GDbProvider *self,
                                                const char *sql,
                                                GListStore *store,
                                                GError **error);

guint g_db_provider_get_id_by_col_name(GDbProvider *self,
                                       const gchar *col_name);

GType g_db_provider_get_type();

G_END_DECLS

#endif /* G_DB_PROVIDER_ */
