/*
   Copyright (c) 2023 Dotsenko A. N.

   This file is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 2.1 of the
   License, or (at your option) any later version.

   This file is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "gdbprovider/gdbrow.h"

static void g_db_row_class_init(GDbRowClass *self_class);
static void g_db_row_init(GDbRow *self);

struct GDbRowPrivate {
    GHashTable *hash;
};

G_DEFINE_TYPE_WITH_PRIVATE(GDbRow, g_db_row, G_TYPE_OBJECT);

enum {
    NONE,
    CHANGED,
    LAST_SIGNAL,
};

static guint db_row_signals[LAST_SIGNAL] = {0};

static void object_finalize(GObject *obj)
{
    GDbRow *self = G_DB_ROW(obj);
    g_hash_table_unref(self->priv->hash);
    G_OBJECT_CLASS(g_db_row_parent_class)->finalize(obj);
}

static void g_db_row_class_init(GDbRowClass *self_class)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS(self_class);
    gobject_class->finalize = object_finalize;

    db_row_signals[CHANGED] = g_signal_new("changed",
                                           G_OBJECT_CLASS_TYPE(gobject_class),
                                           G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                                           0,
                                           NULL,
                                           NULL,
                                           NULL,
                                           G_TYPE_NONE,
                                           0);
}

static void free_value(gpointer value)
{
    if (!value) {
        return;
    }
    g_value_unset(value);
    g_free(value);
}

void g_db_row_set_value(GDbRow *self, guint col_id, const GValue *value)
{
    if (!value) {
        g_hash_table_replace(self->priv->hash, GUINT_TO_POINTER(col_id), NULL);
        return;
    }

    GValue *value_copy = g_new0(GValue, 1);
    g_value_init(value_copy, G_VALUE_TYPE(value));
    g_value_copy(value, value_copy);
    g_hash_table_replace(
            self->priv->hash, GUINT_TO_POINTER(col_id), value_copy);
    g_signal_emit(self, db_row_signals[CHANGED], 0);
}

void g_db_row_set_string_column(GDbRow *self, guint col_id, const gchar *value)
{
    GValue *value_copy = g_new0(GValue, 1);
    g_value_init(value_copy, G_TYPE_STRING);
    g_value_set_string(value_copy, value);
    g_hash_table_replace(
            self->priv->hash, GUINT_TO_POINTER(col_id), value_copy);
    g_signal_emit(self, db_row_signals[CHANGED], 0);
}

void g_db_row_set_int_column(GDbRow *self, guint col_id, int value)
{
    GValue *value_copy = g_new0(GValue, 1);
    g_value_init(value_copy, G_TYPE_INT);
    g_value_set_int(value_copy, value);
    g_hash_table_replace(
            self->priv->hash, GUINT_TO_POINTER(col_id), value_copy);
    g_signal_emit(self, db_row_signals[CHANGED], 0);
}

void g_db_row_set_double_column(GDbRow *self, guint col_id, double value)
{
    GValue *value_copy = g_new0(GValue, 1);
    g_value_init(value_copy, G_TYPE_DOUBLE);
    g_value_set_double(value_copy, value);
    g_hash_table_replace(
            self->priv->hash, GUINT_TO_POINTER(col_id), value_copy);
    g_signal_emit(self, db_row_signals[CHANGED], 0);
}

const GValue *g_db_row_get_value(GDbRow *self, guint col_id)
{
    return g_hash_table_lookup(self->priv->hash, GUINT_TO_POINTER(col_id));
}

gboolean g_db_row_column_is_null(GDbRow *self, guint col_id)
{
    const GValue *value;
    value = g_hash_table_lookup(self->priv->hash, GUINT_TO_POINTER(col_id));
    return (value == NULL);
}

int g_db_row_get_int_column(GDbRow *self, guint col_id)
{
    const GValue *value;
    value = g_hash_table_lookup(self->priv->hash, GUINT_TO_POINTER(col_id));
    if (!value) {
        return 0;
    }
    return g_value_get_int(value);
}

const char *g_db_row_get_string_column(GDbRow *self, guint col_id)
{
    const GValue *value;
    value = g_hash_table_lookup(self->priv->hash, GUINT_TO_POINTER(col_id));
    if (!value) {
        return "";
    }
    return g_value_get_string(value);
}

static void g_db_row_init(GDbRow *self)
{

    self->priv = g_db_row_get_instance_private(self);
    self->priv->hash = g_hash_table_new_full(
            g_direct_hash, g_direct_equal, NULL, free_value);
}

GDbRow *g_db_row_new()
{
    return g_object_new(G_TYPE_DB_ROW, NULL);
}
