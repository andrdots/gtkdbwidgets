/*
   Copyright (c) 2023 Dotsenko A. N.

   This file is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 2.1 of the
   License, or (at your option) any later version.

   This file is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef G_DB_ROW_
#define G_DB_ROW_

#include <glib-object.h>

G_BEGIN_DECLS

#define G_TYPE_DB_ROW (g_db_row_get_type())
#define G_DB_ROW(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), G_TYPE_DB_ROW, GDbRow))
#define G_DB_ROW_CLASS(klass)                                                  \
    (G_TYPE_CHECK_CLASS_CAST((klass), G_TYPE_DB_ROW, GDbRowClass))
#define GTK_IS_DB_ROW(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), G_TYPE_DB_ROW))
#define GTK_IS_DB_ROW_CLASS(klass)                                             \
    (G_TYPE_CHECK_CLASS_TYPE((klass), G_TYPE_DB_ROW))
#define G_DB_ROW_GET_CLASS(obj)                                                \
    (G_TYPE_INSTANCE_GET_CLASS((obj), G_TYPE_DB_ROW, GDbRowClass))

typedef struct GDbRow GDbRow;
typedef struct GDbRowClass GDbRowClass;
typedef struct GDbRowPrivate GDbRowPrivate;

struct GDbRowClass {
    GObjectClass parent_class;
};

struct GDbRow {
    GObject parent;

    GDbRowPrivate *priv;
};

GType g_db_row_get_type();

GDbRow *g_db_row_new();

void g_db_row_set_value(GDbRow *self, guint col_id, const GValue *value);

void g_db_row_set_string_column(GDbRow *self, guint col_id, const gchar *value);

void g_db_row_set_int_column(GDbRow *self, guint col_id, int value);

void g_db_row_set_double_column(GDbRow *self, guint col_id, double value);

const GValue *g_db_row_get_value(GDbRow *self, guint col_id);

gboolean g_db_row_column_is_null(GDbRow *self, guint col_id);

int g_db_row_get_int_column(GDbRow *self, guint col_id);
const char *g_db_row_get_string_column(GDbRow *self, guint col_id);

G_END_DECLS

#endif /* G_DB_ROW_ */
