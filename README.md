# gdbprovider

## Description
The project provides interface to work with databases using GLib library.

GDbProvider provides abstract interface to query database. Custom database providers must inherit GDbProvider and realize its virtual methods. Rows are returned as instances of GDbRow class and can be used to display information in widgets.

## Compiling
1. Clone sources of the project with submodules into a source directory and build the project in the subling build directory:
```shell
mkdir -p ~/gtkdbwidgets/build
cd ~/gtkdbwidgets
git clone --recurse-submodules git@gitlab.com:andrdots/gtkdbwidgets.git ~/gtkdbwidgets/source
cd ~/gtkdbwidgets/build && cmake -DCMAKE_INSTALL_PREFIX=/usr ../source/ && make
```
1. To build deb packages use command:
`cpack -G DEB`

## Using
To use the modules add this library as a submodule into your project and add into CMake with add_subdirectory(). Then link against the library **gtkdbwidgets**.

## Authors and acknowledgment
* Andrey N. Dotsenko

## License
The source code is licensed under the LGPL 2.1 license.
